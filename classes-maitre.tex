%%% Copyright (C) 2024-2025 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «ACT-2002 Classes de maitre»
%%% https://gitlab.com/vigou3/act-2002-classes-maitre
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage[noae]{Sweave}
  \usepackage[mathrm=sym]{unicode-math}  % polices math
  \usepackage{fontawesome5}              % icônes \fa*
  \usepackage{awesomebox}                % \tipbox et autres
  \usepackage{changepage}                % page licence
  \usepackage{tabularx}                  % page licence
  \usepackage{relsize}                   % \smaller et al.
  \usepackage{ifsym}                     % symboles LCD
  \usepackage{booktabs}                  % tableaux
  \usepackage{framed}                    % env. leftbar
  \usepackage[overlay,absolute]{textpos} % couvertures
  \usepackage{metalogo}                  % logo \XeLaTeX
  \usepackage{icomma}                    % virgule intelligente

  %% ============================
  %%  Information de publication
  %% ============================
  \title{ACT-2002 Classes de maitre}
  \author{Vincent Goulet}
  \renewcommand{\year}{2025}
  \renewcommand{\month}{02}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/act-2002-classes-maitre}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Thème Beamer
  \usetheme{metropolis}

  %% Polices de caractères
  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]
  \setmathfont{Fira Math}
  \newfontfamily\titlefontOS{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\titlefontFC{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = Uppercase
  ]

  %% Couleurs
  \definecolor{comments}{rgb}{0.7,0,0} % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}   % liens internes
  \definecolor{url}{rgb}{0.6,0,0}      % liens externes
  \colorlet{codebg}{LightYellow1}      % fond code R
  \definecolor{rouge}{rgb}{0.9,0,0.1}  % bandeau rouge UL
  \definecolor{or}{rgb}{1,0.8,0}       % bandeau or UL
  \definecolor{permitted}{rgb}{0,0.7,0} % vert permission
  \colorlet{forbidden}{comments}        % rouge interdiction
  \colorlet{alert}{mLightBrown}        % alias de couleur Metropolis
  \colorlet{dark}{mDarkTeal}           % alias de couleur Metropolis
  \colorlet{code}{mLightGreen}         % alias de couleur Metropolis
  \colorlet{metropolisbg}{black}       % couleur de fond Metropolis
  \colorlet{shadecolor}{codebg}

  %% Hyperliens
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {IFT-[47]902 Classes de maitre},
    colorlinks = {true},
    linktocpage = {true},
    allcolors = {link},
    urlcolor = {url},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit},
    bookmarksopen = {true},
    bookmarksnumbered = {true},
    bookmarksdepth = {subsection}}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %%% =========================
  %%%  Nouveaux environnements
  %%% =========================

  %% Environnements de Sweave.
  %%
  %% Les environnements Sinput et Soutput utilisent Verbatim (de
  %% fancyvrb). On les réinitialise pour enlever la configuration par
  %% défaut de Sweave, puis on réduit l'écart entre les blocs Sinput
  %% et Soutput.
  \DefineVerbatimEnvironment{Sinput}{Verbatim}{}
  \DefineVerbatimEnvironment{Soutput}{Verbatim}{}
  \fvset{fontsize=\small,listparameters={\setlength{\topsep}{0pt}}}

  %% L'environnement Schunk est complètement redéfini en un hybride
  %% des environnements snugshade* et leftbar de framed.
  \makeatletter
  \renewenvironment{Schunk}{%
    \def\FrameCommand##1{\hskip\@totalleftmargin
      \vrule width 3pt\colorbox{codebg}{\hspace{5pt}##1}%
      % There is no \@totalrightmargin, so:
      \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
    \MakeFramed {\advance\hsize-\width
      \@totalleftmargin\z@ \linewidth\hsize
      \advance\labelsep\fboxsep
      \@setminipage}%
  }{\par\unskip\@minipagefalse\endMakeFramed}
  \makeatother

  %% Boites signalétiques pour les questions
  \newcommand{\questionbox}[1]{%
    \awesomebox{\aweboxrulewidth}{\faMicrophone}{black}{#1}}
  \newcommand{\profbox}[1]{%
    \awesomebox{\aweboxrulewidth}{\faChalkboardTeacher}{black}{#1}}


  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Noms de fonctions, code, environnement, etc.
  \newcommand{\code}[1]{\textcolor{code}{\texttt{#1}}}
  \newcommand{\pkg}[1]{\textbf{#1}}
  \newcommand{\link}[2]{\href{#1}{#2~{\smaller\faExternalLink*}}}
  \newcommand{\ieee}[3]{\fbox{#1}\hspace{2pt}\fbox{#2}\hspace{2pt}\fbox{#3}}

  %% Lien vers GitLab dans la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Identification de la licence CC BY-SA.
  \newcommand{\ccbysa}{\mbox{%
    \faCreativeCommons\kern0.1em%
    \faCreativeCommonsBy\kern0.1em%
    \faCreativeCommonsSa}~\faCopyright[regular]\relax}

  %% Mettre le texte en surbrillance (overlay-aware)
  \newcommand<>{\makealert}[1]{{\color#2{alert}#1}}

  %% Commandes usuelles vgmath
  \newcommand{\pt}{{\scriptscriptstyle\Sigma}}
  \newcommand{\esp}[1]{E [ #1 ]}
  \newcommand{\mat}[1]{\symbf{#1}}
  \newcommand{\diag}{\operatorname{diag}}
  \newcommand{\trsp}{^{\mkern-1.5mu\mathsf{T}}}
  \newcommand{\pscal}[1]{\langle #1 \rangle}

  %%% =======
  %%%  Varia
  %%% =======

  %% Longueurs pour la composition des pages couvertures avant et
  %% arrière.
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}
  \newlength{\logoheight}

% \includeonly{}

\begin{document}

%% frontmatter
\include{couverture-avant}
\include{notices}

%% mainmatter
\include{classe-maitre-1}
\include{classe-maitre-2}
% \include{hors-serie}
% \include{classe-maitre-3}

%% backmatter
\include{colophon}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: t
%%% End:
