%%% Copyright (C) 2024 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «ACT-2002 Classes de maitre»
%%% https://gitlab.com/vigou3/act-2002-classes-maitre
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Classe de maitre hors série}

\begin{frame}[fragile=singleslide]
  \frametitle{Problème}

  La fonction de masse de probabilité de la loi géométrique de
  paramètre $\theta$ est:
  \begin{equation*}
    \Pr[X = x] = \theta (1 - \theta)^x, \quad x = 0, 1, \dots
  \end{equation*}
  On vous donne les valeurs arrondies suivantes:
  \begin{Schunk}
\begin{Sinput}
> dgeom(0:9, 0.35)
\end{Sinput}
\begin{Soutput}
[1] 0.35 0.23 0.15 0.10 0.06 0.04 0.03 0.02 0.01 0.01
\end{Soutput}
  \end{Schunk}
  Simuler cinq ($5$) observations d'une loi géométrique de paramètre
  $\theta = 0,35$ à partir de l'information ci-dessus et du générateur
  congruentiel
  \begin{equation*}
    x_n = 65 x_{n - 1} \bmod \nombre{2048}.
  \end{equation*}
  Utiliser une amorce de $12$.
\end{frame}

\begin{frame}
  \frametitle{Solution}

  Algorithme:
  \begin{enumerate}
  \item Simuler des nombres aléatoires uniformes sur $(0, 1)$
  \item Utiliser la méthode de l'inverse pour une distribution
    discrète
  \end{enumerate}
  \pause

  Ce que ça prend:
  \begin{enumerate}
  \item Des nombres aléatoires uniformes sur $(0, 1)$
  \item Une fonction de répartition
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Solution}

  \begin{itemize}[<+->]
  \item Cinq premières valeurs retournées par le générateur
    $x_n = 65 x_{n - 1} \bmod \nombre{2048}$ avec une amorce de $12$:
    \begin{equation*}
      780 \quad
      1548 \quad
      268 \quad
      1036 \quad
      1804.
    \end{equation*}
  \item Il faut diviser ces nombres par \nombre{2048} pour obtenir des
    nombres dans $(0, 1)$:
    \begin{equation*}
      0,3809 \quad
      0,7559 \quad
      0,1309 \quad
      0,5059 \quad
      0,8809.
    \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Solution}

  \begin{minipage}{0.45\linewidth}
    On nous donne les valeurs de $\Pr[X = x]$ pour $x = 0, \dots, 9$.
    On a donc
    \begin{equation*}
      \Pr[X \leq x] =
      \begin{cases}
        0,35,  & x = 0 \\
        0,58,  & x = 1 \\
        0,73,  & x = 2 \\
        0,83,  & x = 3 \\
        0,89,  & x = 4 \\
        0,93,  & x = 5 \\
        0,96,  & x = 6 \\
        0,98,  & x = 7 \\
        0,99,  & x = 8 \\
        1, & x = 9.
      \end{cases}
    \end{equation*}
  \end{minipage}
  \hfill\pause
  \begin{minipage}{0.45\linewidth}
    Par conséquent:
    \begin{align*}
      u_1 &= 0,3809 &&\Rightarrow& x_1 &= 1 \\
      u_2 &= 0,7559 &&\Rightarrow& x_2 &= 3 \\
      u_3 &= 0,1309 &&\Rightarrow& x_3 &= 0 \\
      u_4 &= 0,5059 &&\Rightarrow& x_4 &= 1 \\
      u_5 &= 0,8809 &&\Rightarrow& x_5 &= 4.
    \end{align*}
  \end{minipage}
\end{frame}


\begin{frame}[fragile=singleslide]
  \frametitle{Problème}

  On vous fournit les valeurs suivantes:
  \begin{Schunk}
\begin{Sinput}
> gamma(c(0.5, 1.5, 2.5))
\end{Sinput}
\begin{Soutput}
[1] 1.77 0.89 1.33
\end{Soutput}
\begin{Sinput}
> rgamma(5, 1.5, 2)
\end{Sinput}
\begin{Soutput}
[1] 1.42 0.26 0.52 0.10 0.45
\end{Soutput}
\begin{Sinput}
> rgamma(5, 1.5, 1)
\end{Sinput}
\begin{Soutput}
[1] 0.451 4.037 0.938 0.209 0.093
\end{Soutput}
  \end{Schunk}
  Évaluer l'intégrale
  \begin{equation*}
    \int_0^\infty x^{7/2} e^{-2x}\, dx
  \end{equation*}
  par la méthode Monte Carlo.
\end{frame}

\begin{frame}
  \frametitle{Principe de l'intégration Monte Carlo}

  Écrire l'intégrale
  \begin{equation*}
    \theta = \int_a^b h(x)\, dx
  \end{equation*}
  sous la forme
  \begin{equation*}
    \theta = \int_a^b g(x) f(x)\, dx,
  \end{equation*}
  où $f(x)$ est une densité sur $(a, b)$. Par définition de
  l'espérance:
  \begin{equation*}
    \theta = \esp{g(X)}.
  \end{equation*}
  Une estimation de la valeur de l'intégrale est
  \begin{equation*}
    \hat{\theta}_n = \frac{1}{n} \sum_{i = 1}^n g(x_i), \quad
    x_1, \dots, x_n \sim f.
  \end{equation*}
\end{frame}

\begin{frame}
  \frametitle{Solution}

  Qu'est-ce que l'on sait?
  \begin{enumerate}
  \item Nombres aléatoires de lois Gamma$(3/2, 2)$ et Gamma$(3/2, 1)$
  \item Fonction dans l'intégrale
    \begin{equation*}
      \int_0^\infty x^{7/2} e^{-2x}\, dx
    \end{equation*}
    ressemble fichtrement à une densité de loi gamma
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Solution}
  Nous avons
  \begin{align*}
    \theta
    &= \int_0^\infty x^{7/2} e^{-2x}\, dx \\
    &= \int_0^\infty x^3 x^{3/2 - 1} e^{-2x}\, dx \\
    &= \frac{\Gamma(3/2)}{2^{3/2}}
      \int_0^\infty x^3 \left( \frac{2^{3/2}}{\Gamma(3/2)} \right) x^{3/2 - 1} e^{-2x}\, dx \\
    &= \frac{\Gamma(3/2)}{2^{3/2}} \esp{X^3},
  \end{align*}
  où $X \sim \text{Gamma}(3/2, 2)$
\end{frame}

\begin{frame}
  \frametitle{Solution}
  Une approximation de l'intégrale est donc
  \begin{equation*}
    \hat{\theta} = \frac{\Gamma(3/2)}{2^{3/2} n} \sum_{i = 1}^n x_i^3,
  \end{equation*}
  où $x_1, \dots, x_n \sim \text{Gamma}(3/2, 2)$.
  \pause

  Nous avons $x_1 = 1,42$, $x_2 = 0,26$, $x_3 = 0,52$,
  $x_4 = 0,10$, $x_5 = 0,45$ et $\Gamma(3/2) = 0,89$.
  \pause

  Par conséquent
  \begin{equation*}
  \hat{\theta} = \frac{0,89}{2^{3/2} (5)} \times 3,1136 =
  0,195946.
  \end{equation*}
\end{frame}


\begin{frame}[fragile=singleslide]
  \frametitle{Problème}

  On vous fournit les nombres aléatoires suivants:
  \begin{Schunk}
\begin{Sinput}
> runif(5)
\end{Sinput}
\begin{Soutput}
[1] 0.20 0.69 0.92 0.28 0.10
\end{Soutput}
  \end{Schunk}

  En utilisant les nombres ci-dessus dans l'ordre, simuler deux
  observations d'un mélange discret entre une exponentielle de moyenne
  $5$ et une exponentielle de moyenne $3$. Le poids dans le mélange de
  la première exponentielle est le double de celui de la seconde.
\end{frame}

\begin{frame}
  \frametitle{Mélange discret \hyperref[diapo:melange-discret]{(redux)}}

  \begin{minipage}[t]{0.45\linewidth}
    \textbf{Définition (deux lois)}
    \begin{equation*}
      F(x) = p_1 F_1(x) + p_2 F_2(x)
    \end{equation*}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.45\linewidth}
    \textbf{Algorithme de simulation}
    \begin{enumerate}
    \item Obtenir un nombre $u$ d'une loi $U(0, 1)$.
    \item Si $u \leq p$, obtenir $x$ de la distribution $F_1$, sinon
      obtenir $x$ de $F_2$.
    \end{enumerate}
  \end{minipage}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Solution}

  \begin{Schunk}
\begin{Sinput}
> runif(5)
\end{Sinput}
\begin{Soutput}
[1] 0.20 0.69 0.92 0.28 0.10
\end{Soutput}
  \end{Schunk}

  \begin{enumerate}[<+->]
  \item $u_1 = 0,20 < 2/3$ donc le premier nombre provient de
    l'exponentielle de moyenne $5$
    \begin{equation*}
      x_1 = -5 \ln 0,69 = 1,855 \text{ ou } x_1 = -5 \ln (1 - 0,69) = 5,856
    \end{equation*}

  \item $u_3 = 0,92 > 2/3$ donc le second nombre provient de
    l'exponentielle de moyenne $3$
    \begin{equation*}
      x_2 = -3 \ln 0,28 = 3,819 \text{ ou } x_2 = -3 \ln (1 - 0,28) = 0,9855
    \end{equation*}
  \end{enumerate}
\end{frame}


\begin{frame}
  \frametitle{Question booléenne}

  Dans un ordinateur, $10$ fois $0,1$ ne donne jamais vraiment $1$.
  \pause

  Pourquoi?
\end{frame}

\begin{frame}
  \frametitle{Question booléenne}

  Dans un ordinateur, $8$ fois $0,125$  donne toujours vraiment $1$.
\end{frame}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: "classes-maitre"
%%% coding: utf-8
%%% End:
